# Bus Schedule API

This is a FastAPI application that returns a bus schedule.

## Endpoints

- `GET /bus_schedule`: Returns a bus schedule.

## Running Locally

1. Install packages:
    ```bash
    pip install -r requirements.txt
    ```

2. Run the application:
    ```bash
    uvicorn apis.main:app --reload
    ```

3. Run the script:
    ```bash
    python apis/main.py
    ```

4. Access the API at `http://127.0.0.1:8080/bus_schedule`.

## Testing

Run tests with pytest:
```bash
pytest --cov=apis
