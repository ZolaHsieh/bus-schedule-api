from fastapi import FastAPI
import uvicorn

app = FastAPI()

@app.get("/bus_schedule")
def get_bus_schedule():
    sample_data = {
        "bus_id": "204",
        "route": "東園 - 麥帥新城",
        "schedule": [
            {"time": "08:00 AM", "stop": "松山車站(八德)"},
            {"time": "08:15 AM", "stop": "松山車站(八德)"},
            {"time": "08:30 AM", "stop": "松山車站(八德)"}
        ]
    }
    return sample_data

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
