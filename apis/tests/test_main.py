from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

def test_get_bus_schedule():
    response = client.get("/bus_schedule")
    assert response.status_code == 200
    data = response.json()
    assert "bus_id" in data
    assert "route" in data
    assert "schedule" in data
    assert len(data["schedule"]) > 0
