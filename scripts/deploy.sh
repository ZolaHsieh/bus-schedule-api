#!/bin/bash

# 檢查是否變數都有設定
if [ -z "$GCP_PROJECT_ID" ] || [ -z "$GCP_CLOUD_RUN_SERVICE" ] || [ -z "$GCP_REGION" ]; then
  echo "Please setup variables required for deploying: GCP_PROJECT_ID, GCP_CLOUD_RUN_SERVICE, GCP_REGION"
  exit 1
fi

# 部署到 GCP Cloud Run
gcloud run deploy "$GCP_CLOUD_RUN_SERVICE" \
  --source ./apis \
  --port 8080 \
  --region $GCP_REGION \
  --project $GCP_PROJECT_ID

echo "Deployed success"
